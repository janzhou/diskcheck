//Author:   Jian Zhou
//Version:  0.1
//Email:    zhoujian@tiaozhanshu.com

package main

import (
    "os"
    "io"
    "fmt"
    "flag"
    "hash"
    "crypto/sha512"
    "crypto/rand"
)

var filesize, checkfrom, checkto, writefrom, writeto int
var debug, fullrun, checkafterwrite bool

func usage(){
    fmt.Fprintf(os.Stderr, "Version: 0.1\n")
    fmt.Fprintf(os.Stderr, "Author: Jian Zhou\n")
    fmt.Fprintf(os.Stderr, "Email: zhoujian@tiaozhanshu.com\n")
    fmt.Fprintf(os.Stderr, "Usage: diskcheck\n")
    flag.PrintDefaults()
    os.Exit(2)
}

func createfile(i int)int{
    filename := fmt.Sprintf("%d", i)
    file, err := os.Create(filename)
    if err != nil {
		fmt.Printf("file create error\n")
		return 1
	}

    var h hash.Hash = sha512.New()
    key := make([]byte, 1024)

    for loop := 0; loop < 1024*filesize - 1; loop++ {
        n, err := io.ReadFull(rand.Reader, key)
        if n < 1024 || err != nil {
            fmt.Printf("createfiel: random value generation error\n")
            return 2
        }

        n, err = file.Write(key)
        if n < 1024 || err != nil {
            fmt.Printf("createfile: writing error\n")
            return 3
        }

        h.Write(key)
    }

    n, err := io.ReadFull(rand.Reader, key)
    if n < 1024 || err != nil {
        fmt.Printf("createfiel: random value generation error\n")
        return 2
    }

    n, err = file.Write(key[0:1024-64])
    if n < 1024-64 || err != nil {
        fmt.Printf("createfile: writing error\n")
        return 3
    }

    h.Write(key[0:1024-64])
    result := h.Sum(nil)

    if debug {
        fmt.Print("hash-write: ", result, "\n")
    }

    n, err = file.Write(result)
    if n < 64 || err != nil {
        fmt.Printf("createfile: hash write error\n")
        return 4
    }

    file.Close()
    return 0
}

func checkfile(i int) int{
    filename := fmt.Sprintf("%d", i)
    file, err := os.Open(filename)
    if err != nil {
        fmt.Printf("checkfile: file open error\n")
		return 1
	}

    var h hash.Hash = sha512.New()
    key := make([]byte, 1024)

    for loop := 0; loop < 1024*filesize - 1; loop++ {
        n, err := file.Read(key)
        if n < 1024 || err != nil {
            fmt.Printf("checkfile: read error\n")
            return 2
        }

        h.Write(key)
    }

    n, err := file.Read(key)
    if n < 1024 || err != nil {
        fmt.Printf("checkfile: read error\n")
        return 2
    }

    n, err = h.Write(key[0:1024-64])

    result := h.Sum(nil)

    if debug {
        fmt.Print("hash-read: ", key[1024-64:1024], "\n")
        fmt.Print("hash-check: ", result, "\n")
    }

    for loop := 0; loop < 64; loop++ {
        if result[loop] != key[1024-64+loop]{
            return 3
        }
    }

    file.Close()
    return 0
}

func main() {
    flag.IntVar(&filesize, "filesize", 1024, "filesize in MB")
    flag.IntVar(&checkfrom, "checkfrom", 0, "checkfrom file number")
    flag.IntVar(&checkto, "checkto", 0, "checkto file number")
    flag.IntVar(&writefrom, "writefrom", 0, "writefrom file number")
    flag.IntVar(&writeto, "writeto", 0, "writeto file number")
    flag.BoolVar(&debug, "debug", false, "use '-debug=true' to print the hash value of the files")
    flag.BoolVar(&fullrun, "fullrun", false, "use '-fullrun=true' to run continously even when the diskcheck encounter an error")
    flag.BoolVar(&checkafterwrite, "checkafterwrite", false, "use '-checkafterwrite=true' to check after write")

    flag.Usage = usage
    flag.Parse()

    if filesize <=0 {
        fmt.Print("File size should not <= 0")
        return
    }

    for i := writefrom; i <= writeto; i++ {
        if 0 != createfile(i) {
            fmt.Printf("File %d write error\n", i)
            if !fullrun {
                break
            }
        }
        if !checkafterwrite {
            fmt.Printf("File %d write success\n", i)
            continue
        }
        if checkfile(i) != 0 {
            fmt.Printf("File %d check error\n", i)
            if !fullrun {
                break
            }
        } else {
            fmt.Printf("File %d check success\n", i)
        }
    }

    for j := checkfrom; j <= checkto; j++ {
        if checkfile(j) != 0 {
            fmt.Printf("File %d check error\n", j)
            if !fullrun {
                break
            }
        } else {
            fmt.Printf("File %d check success\n", j)
        }
    }
}
